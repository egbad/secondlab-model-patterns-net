namespace dotnetcore.Models;

public class Stock
{
    public string Name { get; set; }
    public float Cost { get; set; }
}