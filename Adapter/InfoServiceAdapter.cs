using System;
using dotnetcore.Models;

namespace dotnetcore.Adapter;

public class InfoServiceAdapter
{
    public IInfoService Adaptee;

    public void ConvertToReadable(Stock info)
    {
        Logger.Log("Вызван метод ConvertToReadable у InfoServiceAdapter");
        var convertedInfo = $"{info.Name} теперь стоит {info.Cost} ₽"; 
        Adaptee.TellInfo(convertedInfo);
    }
}