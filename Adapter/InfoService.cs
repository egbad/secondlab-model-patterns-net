using System;
using dotnetcore.Models;

namespace dotnetcore.Adapter;

public interface IInfoService
{
    void TellInfo(string info);
}

public class InfoService : IInfoService
{
    public void TellInfo(string info)
    {
        Logger.Log("Вызван метод TellInfo у InfoService");
        Console.WriteLine(info);
    }
}