using System;
using dotnetcore.Models;
using System.Collections.Generic;
using dotnetcore.Observer.Observers;

namespace dotnetcore.Observer.Observable;

public class Market : IObservable
{
    public List<IObserver> Observers { get; set; }

    private readonly Stock _stock;

    public Market()
    {
        Observers = new List<IObserver>();
        _stock = new Stock();
    }
    
    public void AddObserver(IObserver observer)
    {
        Logger.Log("Вызван метод AddObserver у Market (издатель)");
        Observers.Add(observer);
    }

    public void RemoveObserver(IObserver observer)
    {
        Logger.Log("Вызван метод RemoveObserver у Market (издатель)");
        Observers.Remove(observer);
    }

    public void NotifyObservers()
    {
        Logger.Log("Вызван метод NotifyObservers у Market (издатель)");
        foreach (var observer in Observers)
        {
            observer.Update(_stock);
        }
    }

    public void Trade()
    {
        Logger.Log("Вызван метод Trade у Market (издатель)");
        Random random = new Random();
        _stock.Name = "Доллар";
        _stock.Cost = random.Next(70, 100);
        NotifyObservers();
    }
}