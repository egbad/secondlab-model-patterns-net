using System.Collections.Generic;
using dotnetcore.Observer.Observers;

namespace dotnetcore.Observer.Observable;

public interface IObservable
{
    List<IObserver> Observers { get; set; }
    void AddObserver(IObserver observer);
    void RemoveObserver(IObserver observer);
    void NotifyObservers();
}