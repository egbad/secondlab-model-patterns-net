namespace dotnetcore.Observer.Observers;

public interface IObserver
{
    void Update(object obj);
}