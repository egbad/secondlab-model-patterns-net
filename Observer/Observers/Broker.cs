using System;
using dotnetcore.Models;
using dotnetcore.Adapter;
using dotnetcore.Observer.Observable;

namespace dotnetcore.Observer.Observers;

public class Broker : IObserver
{
    public String Name { get; set; }
    private IObservable _stock;
    private InfoServiceAdapter _infoServiceInfoServiceAdapter;
    
    public Broker(string name, IObservable observable, InfoServiceAdapter adapter)
    {
        Name = name;
        _stock = observable;
        _stock.AddObserver(this);
        _infoServiceInfoServiceAdapter = adapter;
    }
    
    public void Update(object obj)
    {
        Logger.Log("Вызван метод Update у Broker (подписчик)");
        var stockInfo = obj as Stock;
        _infoServiceInfoServiceAdapter.ConvertToReadable(stockInfo);
    }

    public void StopTrade()
    {
        Logger.Log("Вызван метод StopTrade у Broker (подписчик)");
        _stock.RemoveObserver(this);
        _stock = null;
    }
}