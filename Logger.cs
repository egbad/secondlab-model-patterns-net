using System;

namespace dotnetcore;

public static class Logger
{
    public static void Log(string log)
    {
        if (Program.IsDebugMode)
        {
            Console.WriteLine(log);
        }
    }
}