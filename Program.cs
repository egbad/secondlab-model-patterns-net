﻿using dotnetcore.Adapter;
using dotnetcore.Observer.Observers;
using dotnetcore.Observer.Observable;

namespace dotnetcore
{
    class Program
    {
        // Позволяет вывести в консоль детальную информацию по вызову методов
        public const bool IsDebugMode = true;

        static void Main(string[] args)
        {
            var infoService = new InfoService();
            var infoServiceAdapter = new InfoServiceAdapter();
            infoServiceAdapter.Adaptee = infoService;
            
            var stock = new Market();
            var broker = new Broker("Тинькофф Инвестиции", stock, infoServiceAdapter);

            // Вызываем у биржи метод Trade, чтобы прошли торги
            stock.Trade(); // Доллар теперь стоит 73 ₽
            stock.Trade(); // Доллар теперь стоит 81 ₽
            // После каждого вызова у класса Broker отработал метод Update (т.к. он слушатель у класса Market)
            // Затем адаптер (класс InfoServiceAdapter) конвертировал модель данных в читабельный вид
            // А сервис (класс InfoService) вывел информацию в консоль

            // Отписываем подписчика (Broker) от издателя (Market) 
            broker.StopTrade();
            
            // Теперь никакой информации не выводится
            stock.Trade(); // ...
            stock.Trade(); // ...
        }
    }
}
